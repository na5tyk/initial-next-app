const STORAGE = {
  ACCESS_TOKEN: "access",
  REFRESH_TOKEN: "refresh",
};

export default STORAGE;

const API = {
  AUTH_LOGIN: '/auth/jwt/create/',
  AUTH_JWT_REFRESH: '/auth/jwt/refresh/',
}

export default API;
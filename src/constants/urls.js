const URLS = {
  HOME_PAGE: "/",
  LOGIN_PAGE: "/login",
};

export default URLS;

import axios from "axios";

import API from "src/constants/api";
import STORAGE from "src/constants/storage";
import URLS from "src/constants/urls";

import removeAuthCookies from "../removeAuthCookies";
import { setCookie } from "../setCookie";

const redirect = (res) => {
  if (res) {
    res.statusCode = 302;
    setCookie(res, STORAGE.ACCESS_TOKEN, "", { maxAge: 0 });
    setCookie(res, STORAGE.REFRESH_TOKEN, "", { maxAge: 0 });
    res.setHeader("Location", URLS.LOGIN_PAGE);
  }
};

const refreshToken = async (res, rToken, removeAuth = true) => {
  const axiosInstance = axios.create();
  axiosInstance.defaults.baseURL = process.env.NEXT_PUBLIC_API_URL;

  let data;

  if (!rToken) {
    redirect(res);
    return;
  }

  try {
    data = await axiosInstance.post(API.AUTH_JWT_REFRESH, {
      refresh: rToken,
    });
    setCookie(res, STORAGE.ACCESS_TOKEN, data.data.access, { maxAge: 0 });
  } catch (err) {
    if (removeAuth) {
      removeAuthCookies(res);
      redirect(res);
    }
    return null;
  }

  return data;
};

export default refreshToken;

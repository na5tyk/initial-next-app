import cookie from "js-cookie";
import { setCookie } from "../setCookie";
import STORAGE from "src/constants/storage";

const removeAuthCookies = (res) => {
  if (res) {
    setCookie(res, STORAGE.ACCESS_TOKEN, null);
    setCookie(res, STORAGE.REFRESH_TOKEN, null);
  } else {
    cookie.remove(STORAGE.ACCESS_TOKEN);
    cookie.remove(STORAGE.REFRESH_TOKEN);
  }
};

export default removeAuthCookies;

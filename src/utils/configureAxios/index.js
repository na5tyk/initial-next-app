import axios from "axios";
import { getSession } from "next-auth/client";
import { toast } from "react-toastify";
import cookie from "js-cookie";

import parseResponseError from "../parseResponseError";
import refreshToken from "../refreshToken";
import removeAuthCookies from "../removeAuthCookies";

import STORAGE from "src/constants/storage";
import URLS from "src/constants/urls";

export const handleError = async (error) => {
  const rToken = cookie.get(STORAGE.REFRESH_TOKEN);

  if (error.response && error.response.data) {
    if (error.response.status === 429) {
      toast.error(parseResponseError(error.response.data.message));
    }

    // Obsługa przeterminowanego tokena.
    if (error.response.status === 401) {
      try {
        if (!rToken || rToken?.includes("null")) throw Error("Błędny token");

        const { data } = await refreshToken(null, rToken);
        const request = error.config;

        if (!request._retry) {
          request._retry = true;

          try {
            request.headers.Authorization = `JWT ${data.access}`;
            axios.defaults.headers["Authorization"] = `JWT ${data.access}`;
            cookie.set(STORAGE.ACCESS_TOKEN, data.access);

            const retry = await axios(request);
            return Promise.resolve(retry);
          } catch (err) {
            return Promise.reject(err);
          }
        } else {
          throw error;
        }
      } catch (err) {
        const { pathname } = window.location;
        removeAuthCookies();
        if (!(pathname === URLS.HOME_PAGE || pathname === URLS.LOGIN_PAGE))
          window.location.href = "/";
      }
    }
  }

  return Promise.reject(error);
};

const configureAxios = () => {
  const CancelToken = axios.CancelToken;
  let cancel;
  axios.defaults.baseURL = process.env.NEXT_PUBLIC_API_URL;

  axios.interceptors.response.use(
    (response) => response,
    async (error) => {
      const response = await handleError(error);
      return response;
    }
  );

  axios.interceptors.request.use(async (config) => {
    const session = await getSession();

    if (session?.accessToken)
      config.headers.Authorization = `JWT ${session?.accessToken}`;

    config.cancelToken = new CancelToken((cancelParam) => {
      cancel = cancelParam;
    });

    return config;
  });
};

export default configureAxios;

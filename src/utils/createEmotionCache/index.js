import createCache from "@emotion/cache";

const createEmotionCache = () => {
  // TODO remove prepend: true once JSS is out
  return createCache({ key: "css", prepend: true });
};

export default createEmotionCache;

import { serialize } from "cookie";

export const setCookie = (res, name, value, options) => {
  const stringValue =
    typeof value === "object" ? "j:" + JSON.stringify(value) : String(value);

  if (options?.maxAge) {
    options.expires = new Date(Date.now() + options.maxAge);
    options.maxAge /= 1000;
  }

  options.sameSite = "strict";

  if (res)
    res.setHeader("Set-Cookie", serialize(name, String(stringValue), options));
};

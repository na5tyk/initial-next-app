const initialState = {
  example: true,
};

const globalReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    default:
      return state;
  }
};

export default globalReducer;

import axios from "axios";
import NextAuth from "next-auth";
import Providers from "next-auth/providers";

import API from "src/constants/api";
import parseResponseError from "src/utils/parseResponseError";

const expired = 5 * 50000; //5 minut

const refreshAccessToken = async (prevToken) => {
  const _axios = axios.create();
  const link = process.env.API_URL || process.env.NEXT_PUBLIC_API_URL;

  const token = await _axios.post(`${link}${API.AUTH_JWT_REFRESH}`, {
    refresh: prevToken.refreshToken,
  });

  const userData = await _axios.get(
    `${process.env.API_URL}${API.AUTH_USERS_ME}`,
    {
      headers: {
        Authorization: `JWT ${token.data.access}`,
      },
    }
  );

  return {
    accessToken: token.data.access,
    refreshToken: prevToken.refreshToken,
    user: userData?.data,
    accessTokenExpires: Date.now() + expired,
  };
};

export default NextAuth({
  pages: {
    signIn: "/login",
    error: "/login",
  },
  providers: [
    Providers.Credentials({
      id: "credentials",
      name: "Credentials",
      credentials: {
        username: { label: "Username", type: "text" },
        password: { label: "Password", type: "password" },
      },
      authorize: async (credentials) => {
        const _axios = axios.create();
        let token, userData;

        try {
          token = await _axios.post(
            `${process.env.API_URL}${API.AUTH_LOGIN}`,
            credentials
          );
        } catch (err) {
          throw new Error(parseResponseError(err));
        }

        try {
          userData = await _axios.get(
            `${process.env.API_URL}${API_AUTH_USERS_ME}`,
            {
              headers: {
                Authorization: `JWT ${token.data.access}`,
              },
            }
          );
        } catch (err) {
          throw new Error(parseResponseError(err));
        }

        return {
          token: token.data,
          userData: userData.data,
        };
      },
    }),
  ],
  session: {
    jwt: true,
  },
  callbacks: {
    jwt: async (prevToken, token) => {
      if (token?.token) {
        return {
          accessToken: token?.token?.access,
          refreshToken: token?.token?.refresh,
          user: token?.userData,
          accessTokenExpires: Date.now() + expired,
        };
      }

      if (Date.now() < prevToken.accessTokenExpires) {
        return prevToken;
      }

      return refreshAccessToken(prevToken);
    },
    session: async (session, token) => {
      if (token.accessToken) {
        session.accessToken = token.accessToken;
      } else {
        try {
          const { accessToken } = await refreshAccessToken(token);
          if (accessToken) {
            session.accessToken = accessToken;
          }
        } catch (err) {
          return null;
        }
      }

      if (token.refreshToken) session.refreshToken = token.refreshToken;
      if (token.user) session.user = token.user;

      return session;
    },
    redirect: async (url, baseUrl) => {
      return baseUrl;
    },
  },
});

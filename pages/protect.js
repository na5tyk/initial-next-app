import React from "react";
import { getSession } from "next-auth/client";
import { useRouter } from "next/router";

import URLS from "src/constants/urls";

const ProtectPage = ({ session }) => {
  const router = useRouter();

  if (typeof window !== "undefined" && !session?.user) {
    router.push(URLS.LOGIN_PAGE);
    return null;
  }

  return <div>Zabezpieczone</div>;
};

export async function getServerSideProps(context) {
  const session = await getSession(context);
  return {
    props: { session },
  };
}

export default ProtectPage;

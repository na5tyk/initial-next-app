import React, { useEffect } from "react";
import { Provider as ReduxProvider } from "react-redux";
import { Hydrate, QueryClient, QueryClientProvider } from "react-query";

import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider } from "@emotion/react";

import { Provider } from "next-auth/client";
import Head from "next/head";
import Router from "next/router";
import NProgress from "nprogress";

import configureAxios from "src/utils/configureAxios";
import createEmotionCache from "src/utils/createEmotionCache";
import theme from "src/utils/theme";

import { useStore } from "src/store/store";

import "react-toastify/dist/ReactToastify.css";

const clientSideEmotionCache = createEmotionCache();

configureAxios();

const MyApp = ({
  Component,
  pageProps,
  emotionCache = clientSideEmotionCache,
}) => {
  const [queryClient] = React.useState(() => new QueryClient());
  const store = useStore(pageProps.initialReduxState);

  useEffect(() => {
    NProgress.configure({ showSpinner: false });

    Router.events.on("routeChangeStart", () => {
      NProgress.start();
    });

    Router.events.on("routeChangeComplete", () => {
      NProgress.done();
    });

    Router.events.on("routeChangeError", () => {
      NProgress.done();
    });
  }, []);

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta charSet="UTF-8" />

        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Provider session={pageProps.session}>
          <ReduxProvider store={store}>
            <QueryClientProvider client={queryClient}>
              <Hydrate state={pageProps.dehydratedState}>
                <Component {...pageProps} />
              </Hydrate>
            </QueryClientProvider>
          </ReduxProvider>
        </Provider>
      </ThemeProvider>
    </CacheProvider>
  );
};

export default MyApp;
